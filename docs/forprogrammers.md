Purpose
=======

This document provides a pretty good map for programmers to understand the terminology of englike if you already know some c-like language (c, java, c++, c#, etc.)

A "noun" is kind of like a class.
a "thing" is kind of like an object.
a "verb" is kind of like a function.
a "sentence" is kind of like a statement.