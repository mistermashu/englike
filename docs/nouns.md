Purpose
=======

Nouns allow you to tell the computer what a noun is.

What a noun is is simply what sorts of things the computer can know about any given thing that is a noun.
