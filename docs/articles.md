Purpose
=======

Articles can be used in a few ways:


Before a noun when making a thing
--------------------------------

When using an article while making a thing, the following articles perform the following functions:
	- "a": tells the computer that you want to make a thing. this is default.
	- "the": tells the computer that you want to initialize the only thing that is the given noun.
	- "some": tells the computer you are making a list of things that are the given noun.