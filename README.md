General
=======

englike is a programming language for teaching computers in an english-like language.  the name "englike" comes from replacing the diminuitive suffix "ish" with its more positive counterpart "like"

where other programming languages are childish, englike is childlike, being young and pure.

the purpose of englike is not to be able to make anybody be able to program anything by knowing english, but rather to take one step closer to having a completely natural programming language that is equally as efficient as any low level language.

this project consists of my documentation, and if it gets far enough, my implementation of a compiler, which I imagine will compile to either native assembly or LLVM.


Contributors
===========

So far it is just me (mistermashu.com)